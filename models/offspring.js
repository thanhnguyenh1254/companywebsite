var moment = require('moment');
var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var OffspringSchema = new Schema(
  {
    employee: { type: Schema.ObjectId, ref: 'Employee', required: true }, //reference to the associated parent
    first_name: {type: String, required: true, max: 100},
    date_of_birth: {type: Date}
  }
);

// Virtual for offspring's URL
OffspringSchema
.virtual('url')
.get(function () {
  return '/catalog/offspring/' + this._id;
});

OffspringSchema
.virtual('date_of_birth_formatted')
.get(function () {
  return this.date_of_birth ? moment(this.date_of_birth).format('MMMM Do, YYYY') : '';
});

//Export model
module.exports = mongoose.model('Offspring', OffspringSchema);
