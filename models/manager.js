var moment = require('moment');
var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var ManagerSchema = new Schema(
  {
    first_name: {type: String, required: true, max: 100},
    family_name: {type: String, required: true, max: 100},
    date_of_birth: {type: Date},
    department: [{type: Schema.ObjectId, ref: 'Department'}]
  }
);

// Virtual for manager's full name
ManagerSchema
.virtual('name')
.get(function () {
  return this.family_name + ', ' + this.first_name;
});

// Virtual for manager's URL
ManagerSchema
.virtual('url')
.get(function () {
  return '/catalog/manager/' + this._id;
});

ManagerSchema
.virtual('date_of_birth_formatted')
.get(function () {
  return this.date_of_birth ? moment(this.date_of_birth).format('MMMM Do, YYYY') : '';
});

//Export model
module.exports = mongoose.model('Manager', ManagerSchema);
