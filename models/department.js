var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var DepartmentSchema = new Schema(
  {
    name: {type: String, required: true, min: 1, max: 100}
  }
);

DepartmentSchema
.virtual('url')
.get(function () {
  return '/catalog/department/' + this._id;
});

module.exports = mongoose.model('Department', DepartmentSchema);
