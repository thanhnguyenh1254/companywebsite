var moment = require('moment');
var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var EmployeeSchema = new Schema(
  {
    first_name: {type: String, required: true, max: 100},
    family_name: {type: String, required: true, max: 100},
    manager: {type: Schema.ObjectId, ref: 'Manager', required: true},
    date_of_birth: {type: Date},
    employee_number: {type: String, required: true},
    department: [{type: Schema.ObjectId, ref: 'Department'}]
  }
);

// Virtual for employee's full name
EmployeeSchema
.virtual('name')
.get(function () {
  return this.family_name + ', ' + this.first_name;
});

// Virtual for employee's URL
EmployeeSchema
.virtual('url')
.get(function () {
  return '/catalog/employee/' + this._id;
});

EmployeeSchema
.virtual('date_of_birth_formatted')
.get(function () {
  return this.date_of_birth ? moment(this.date_of_birth).format('MMMM Do, YYYY') : '';
});

//Export model
module.exports = mongoose.model('Employee', EmployeeSchema);
