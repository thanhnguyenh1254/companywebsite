function changePage() {
  var pathName = window.location.pathname;
  var routes = {
    '^/catalog$':                           'btn1',
    '^/catalog/employee((?!create).)*$':    'btn2',
    '^/catalog/manager((?!create).)*$':     'btn3',
    '^/catalog/department((?!create).)*$':  'btn4',
    '^/catalog/offspring((?!create).)*$':   'btn5',
    '^/catalog/manager/create$':            'btn6',
    '^/catalog/department/create$':         'btn7',
    '^/catalog/employee/create$':           'btn8',
    '^/catalog/offspring/create$':          'btn9'
  }

  var buttonID;
  Object.keys(routes).forEach(function(key) {
    if (RegExp(key).test(pathName)) {
      buttonID = '#' + routes[key];
    }
  });

  $(buttonID).attr('class', ' active');
}

function showTime() {
  var date = new Date();
  var time = date.toLocaleTimeString('en-US');

  $('#myClockDisplay')[0].innerText = (time.length < 11) ? ('0' + time) : (time);

  setTimeout(showTime, 1000);
}

window.addEventListener('load', function() {
  changePage();
  showTime();
});
