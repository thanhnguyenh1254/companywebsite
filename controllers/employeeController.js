const { body,validationResult } = require('express-validator/check');
const { sanitizeBody } = require('express-validator/filter');

var Employee = require('../models/employee');
var Manager = require('../models/manager');
var Department = require('../models/department');
var Offspring = require('../models/offspring');

var async = require('async');

exports.index = function(req, res) {

    async.parallel({
        employee_count: function(callback) {
            Employee.count({}, callback); // Pass an empty object as match condition to find all documents of this collection
        },
        off_spring_count: function(callback) {
            Offspring.count({}, callback);
        },
        manager_count: function(callback) {
            Manager.count({}, callback);
        },
        department_count: function(callback) {
            Department.count({}, callback);
        },
    }, function(err, results) {
        res.render('index', { title: 'Company Website Home', error: err, data: results });
    });
};

// Display list of all Employees.
exports.employee_list = function(req, res, next) {

  Employee.find({}, 'first_name family_name')
    .populate('manager')
    .populate('department')
    .exec(function (err, list_employees) {
      if (err) { return next(err); }
      //Successful, so render
      res.render('employee_list', { title: 'Employee List', employee_list: list_employees });
    });

};

// Display detail page for a specific employee.
exports.employee_detail = function(req, res, next) {

    async.parallel({
        employee: function(callback) {

            Employee.findById(req.params.id)
              .populate('manager')
              .populate('department')
              .exec(callback);
        },
        off_spring: function(callback) {

          Offspring.find({ 'employee': req.params.id })
          .exec(callback);
        },
    }, function(err, results) {
        if (err) { return next(err); }
        if (results.employee==null) { // No results.
            var err = new Error('Employee not found');
            err.status = 404;
            return next(err);
        }
        // Successful, so render.
        res.render('employee_detail', { title: 'Title', employee:  results.employee, off_springs: results.off_spring } );
    });

};

// Display employee create form on GET.
exports.employee_create_get = function(req, res, next) {

    // Get all managers and departments, which we can use for adding to our employee.
    async.parallel({
        managers: function(callback) {
            Manager.find(callback);
        },
        departments: function(callback) {
            Department.find(callback);
        },
    }, function(err, results) {
        if (err) { return next(err); }
        res.render('employee_form', { title: 'Create Employee', managers: results.managers, departments: results.departments });
    });

};

// Handle employee create on POST.
exports.employee_create_post = [
    // Convert the department to an array.
    (req, res, next) => {
        if(!(req.body.department instanceof Array)){
            if(typeof req.body.department==='undefined')
            req.body.department=[];
            else
            req.body.department=new Array(req.body.department);
        }
        next();
    },

    // Validate fields.
    body('first_name', 'First name must not be empty.').isLength({ min: 1 }).trim(),
    body('family_name', 'Family name must not be empty.').isLength({ min: 1 }).trim(),
    body('manager', 'Manager must not be empty.').isLength({ min: 1 }).trim(),
    body('employee_number', 'Employee number must not be empty').isLength({ min: 1 }).trim(),

    // Sanitize fields (using wildcard).
    sanitizeBody('*').trim().escape(),

    // Process request after validation and sanitization.
    (req, res, next) => {

        // Extract the validation errors from a request.
        const errors = validationResult(req);

        // Create a Employee object with escaped and trimmed data.
        var employee = new Employee(
          { first_name: req.body.first_name,
            family_name: req.body.family_name,
            manager: req.body.manager,
            date_of_birth: req.body.date_of_birth,
            employee_number: req.body.employee_number,
            department: req.body.department
           });

        if (!errors.isEmpty()) {
            // There are errors. Render form again with sanitized values/error messages.

            // Get all managers and departments for form.
            async.parallel({
                managers: function(callback) {
                    Manager.find(callback);
                },
                departments: function(callback) {
                    Department.find(callback);
                },
            }, function(err, results) {
                if (err) { return next(err); }

                // Mark our selected departments as checked.
                for (let i = 0; i < results.departments.length; i++) {
                    if (employee.department.indexOf(results.departments[i]._id) > -1) {
                        results.departments[i].checked='true';
                    }
                }
                res.render('employee_form', { title: 'Create Employee',managers:results.managers, departments:results.departments, employee: employee, errors: errors.array() });
            });
            return;
        }
        else {
            // Data from form is valid. Save employee.
            employee.save(function (err) {
                if (err) { return next(err); }
                   //successful - redirect to new employee record.
                   res.redirect(employee.url);
                });
        }
    }
];

// Display employee delete form on GET.
exports.employee_delete_get = function(req, res, next) {

    async.parallel({
      employee: function(callback) {
        Employee.findById(req.params.id).exec(callback)
      },
      employees_offsprings: function(callback) {
        Offspring.find({ 'employee': req.params.id }).exec(callback)
      },
    }, function (err, results) {
        if (err) { return next(err); }
        if (results.employee==null) {
          res.redirect('/catalog/employees');
        }

        res.render('employee_delete', { title: 'Delete Employee', employee: results.employee, employees_offsprings: results.employees_offsprings });
    });

};

// Handle employee delete on POST.
exports.employee_delete_post = function(req, res, next) {

  async.parallel({
    employee: function(callback) {
      Employee.findById(req.body.employeeid).exec(callback)
    },
    employees_offsprings: function(callback) {
      Offspring.find({ 'employee': req.params.employeeid }).exec(callback)
    },
  }, function(err, results) {
    if (err) { return next(err); }
    if (results.employees_offsprings.length > 0) {
      res.reder('employee_delete', { title: 'Delete Employee', employee: results.employee, employees_offsprings: results.employees_offsprings } );
      return;
    }
    else {
      Employee.findByIdAndRemove(req.body.employeeid, function deleteEmployee(err) {
        if (err) { return next(err); }
        res.redirect('/catalog/employees')
      })
    }
  });

};

// Display employee update form on GET.
exports.employee_update_get = function(req, res, next) {

    // Get employee, managers and departments for form.
    async.parallel({
        employee: function(callback) {
            Employee.findById(req.params.id).populate('manager').populate('department').exec(callback);
        },
        managers: function(callback) {
            Manager.find(callback);
        },
        departments: function(callback) {
            Department.find(callback);
        },
        }, function(err, results) {
            if (err) { return next(err); }
            if (results.employee==null) { // No results.
                var err = new Error('Employee not found');
                err.status = 404;
                return next(err);
            }
            // Success.
            // Mark our selected departments as checked.
            for (var all_d_iter = 0; all_d_iter < results.departments.length; all_d_iter++) {
                for (var employee_d_iter = 0; employee_d_iter < results.employee.department.length; employee_d_iter++) {
                    if (results.departments[all_d_iter]._id.toString()==results.employee.department[employee_d_iter]._id.toString()) {
                        results.departments[all_d_iter].checked='true';
                    }
                }
            }
            res.render('employee_form', { title: 'Update Employee', managers:results.managers, departments:results.departments, employee: results.employee });
        });

};

// Handle employee update on POST.
exports.employee_update_post = [

    // Convert the department to an array
    (req, res, next) => {
        if(!(req.body.department instanceof Array)){
            if(typeof req.body.department==='undefined')
            req.body.department=[];
            else
            req.body.department=new Array(req.body.department);
        }
        next();
    },

    // Validate fields.
    body('first_name', 'First name must not be empty.').isLength({ min: 1 }).trim(),
    body('family_name', 'Family name must not be empty.').isLength({ min: 1 }).trim(),
    body('manager', 'Summary must not be empty.').isLength({ min: 1 }).trim(),
    body('employee_number', 'Employee number must not be empty').isLength({ min: 1 }).trim(),

    // Sanitize fields.
    sanitizeBody('first_name').trim().escape(),
    sanitizeBody('family_name').trim().escape(),
    sanitizeBody('manager').trim().escape(),
    sanitizeBody('employee_number').trim().escape(),
    sanitizeBody('department.*').trim().escape(),

    // Process request after validation and sanitization.
    (req, res, next) => {

        // Extract the validation errors from a request.
        const errors = validationResult(req);

        // Create a Employee object with escaped/trimmed data and old id.
        var employee = new Employee(
          { first_name: req.body.first_name,
            family_name: req.body.family_name,
            manager: req.body.manager,
            date_of_birth: req.body.date_of_birth,
            employee_number: req.body.employee_number,
            department: (typeof req.body.department==='undefined') ? [] : req.body.department,
            _id:req.params.id //This is required, or a new ID will be assigned!
           });

        if (!errors.isEmpty()) {
            // There are errors. Render form again with sanitized values/error messages.

            // Get all managers and departments for form.
            async.parallel({
                managers: function(callback) {
                    Manager.find(callback);
                },
                departments: function(callback) {
                    Department.find(callback);
                },
            }, function(err, results) {
                if (err) { return next(err); }

                // Mark our selected departments as checked.
                for (let i = 0; i < results.departments.length; i++) {
                    if (employee.department.indexOf(results.departments[i]._id) > -1) {
                        results.departments[i].checked='true';
                    }
                }
                res.render('employee_form', { title: 'Update Employee',managers:results.managers, departments:results.departments, employee: employee, errors: errors.array() });
            });
            return;
        }
        else {
            // Data from form is valid. Update the record.
            Employee.findByIdAndUpdate(req.params.id, employee, {}, function (err,theemployee) {
                if (err) { return next(err); }
                   // Successful - redirect to employee detail page.
                   res.redirect(theemployee.url);
                });
        }
    }
];
