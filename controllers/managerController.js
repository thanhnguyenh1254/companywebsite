const { body,validationResult } = require('express-validator/check');
const { sanitizeBody } = require('express-validator/filter');

var async = require('async');
var Employee = require('../models/employee');
var Manager = require('../models/manager');
var Department = require('../models/department');

// Display list of all Authors.
exports.manager_list = function(req, res, next) {

  Manager.find()
    .sort([['family_name', 'ascending']])
    .exec(function (err, list_managers) {
      if (err) { return next(err); }
      //Successful, so render
      res.render('manager_list', { title: 'Manager List', manager_list: list_managers });
    });

};

// Display detail page for a specific Author.
exports.manager_detail = function(req, res, next) {

    async.parallel({
        manager: function(callback) {
            Manager.findById(req.params.id)
              .populate('department')
              .exec(callback)
        },
        managers_employees: function(callback) {
          Employee.find({ 'manager': req.params.id },'first_name family_name')
          .exec(callback)
        },
    }, function(err, results) {
        if (err) { return next(err); } // Error in API usage.
        if (results.manager==null) { // No results.
            var err = new Error('Manager not found');
            err.status = 404;
            return next(err);
        }
        // Successful, so render.
        res.render('manager_detail', { title: 'Manager Detail', manager: results.manager, manager_employees: results.managers_employees } );
    });

};

// Display Author create form on GET.
exports.manager_create_get = function(req, res, next) {

    async.parallel({
      departments: function(callback) {
          Department.find(callback);
      },
    }, function (err, results) {
      if (err) { return next(err); }
      res.render('manager_form', { title: 'Create Manager', departments: results.departments });
    });

};

// Handle Author create on POST.
exports.manager_create_post = [

    // Convert the department to an array.
    (req, res, next) => {
        if(!(req.body.department instanceof Array)){
            if(typeof req.body.department==='undefined')
            req.body.department=[];
            else
            req.body.department=new Array(req.body.department);
        }
        next();
    },

    // Validate fields.
    body('first_name').isLength({ min: 1 }).trim().withMessage('First name must be specified.')
        .isAlphanumeric().withMessage('First name has non-alphanumeric characters.'),
    body('family_name').isLength({ min: 1 }).trim().withMessage('Family name must be specified.')
        .isAlphanumeric().withMessage('Family name has non-alphanumeric characters.'),
    body('date_of_birth', 'Invalid date of birth').optional({ checkFalsy: true }).isISO8601(),

    // Sanitize fields.
    sanitizeBody('first_name').trim().escape(),
    sanitizeBody('family_name').trim().escape(),
    sanitizeBody('date_of_birth').toDate(),

    // Process request after validation and sanitization.
    (req, res, next) => {

        // Extract the validation errors from a request.
        const errors = validationResult(req);

        // Create an Author object with escaped and trimmed data.
        var manager = new Manager(
            {
                first_name: req.body.first_name,
                family_name: req.body.family_name,
                date_of_birth: req.body.date_of_birth,
                department: req.body.department
            });

        if (!errors.isEmpty()) {
            // There are errors. Render form again with sanitized values/errors messages.

            async.parallel({
              departments: function(callback) {
                  Department.find(callback);
              },
            }, function(err, results) {
              if (err) { return next(err); }

              for (let i = 0; i < results.departments.length; i++) {
                  if (manager.department.indexOf(results.departments[i]._id) > -1) {
                      results.departments[i].checked='true';
                  }
              }
              res.render('manager_form', { title: 'Create Manager', manager: manager, departments:results.departments, errors: errors.array() });

            });
            return;

        }
        else {
            // Data from form is valid.

            manager.save(function (err) {
                if (err) { return next(err); }
                // Successful - redirect to new author record.
                res.redirect(manager.url);
            });
        }
    }
];

// Display Author delete form on GET.
exports.manager_delete_get = function(req, res, next) {

    async.parallel({
        manager: function(callback) {
            Manager.findById(req.params.id).exec(callback)
        },
        managers_employees: function(callback) {
          Employee.find({ 'manager': req.params.id }).exec(callback)
        },
    }, function(err, results) {
        if (err) { return next(err); }
        if (results.manager==null) { // No results.
            res.redirect('/catalog/managers');
        }
        // Successful, so render.
        res.render('manager_delete', { title: 'Delete Manager', manager: results.manager, manager_employees: results.managers_employees } );
    });

};

// Handle Author delete on POST.
exports.manager_delete_post = function(req, res, next) {

    async.parallel({
        manager: function(callback) {
          Manager.findById(req.body.authorid).exec(callback)
        },
        managers_employees: function(callback) {
          Employee.find({ 'manager': req.body.managerid }).exec(callback)
        },
    }, function(err, results) {
        if (err) { return next(err); }
        // Success
        if (results.managers_employees.length > 0) {
            // Author has books. Render in same way as for GET route.
            res.render('manager_delete', { title: 'Delete Manager', manager: results.manager, manager_employees: results.managers_employees } );
            return;
        }
        else {
            // Author has no books. Delete object and redirect to the list of authors.
            Manager.findByIdAndRemove(req.body.managerid, function deleteManager(err) {
                if (err) { return next(err); }
                // Success - go to author list
                res.redirect('/catalog/managers')
            })
        }
    });
};

// Display Author update form on GET.
exports.manager_update_get = function(req, res, next) {

    async.parallel({
      manager: function(callback) {
        Manager.findById(req.params.id).exec(callback);
      },
    }, function (err, results) {
      if (err) { return next(err); }
      if (results.manager==null) {
        var err = new Error('Manager not found');
        err.status = 404;
        return next(err);
      }

      res.render('manager_form', { title: 'Update Manager', manager: results.manager});
    });

};

// Handle Author update on POST.
exports.manager_update_post = [

  body('first_name', 'First name must not be empty').isLength({ min: 1}).trim(),
  body('family_name', 'Family name must not be empty').isLength({ min: 1}).trim(),

  sanitizeBody('first_name').trim().escape(),
  sanitizeBody('family_name').trim().escape(),

  (req, res, next) => {

    const errors = validationResult(req);

    var manager = new Manager(
      { first_name: req.body.first_name,
        family_name: req.body.family_name,
        date_of_birth: req.body.date_of_birth,
        department: req.body.department,
        _id: req.params.id
      });

      if (!errors.isEmpty()) {
        res.render('manager_form', {title: 'Update Manager', manager: manager, errors: errors.array() });
        return;
      }
      else {
        Manager.findByIdAndUpdate(req.params.id, manager, {}, function (err,themanager) {
          if (err) { return next(err); }
          res.redirect(themanager.url);
        });
      }
  }

];
