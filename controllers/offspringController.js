const { body,validationResult } = require('express-validator/check');
const { sanitizeBody } = require('express-validator/filter');

var async = require('async');
var Employee = require('../models/employee');
var Offspring = require('../models/offspring');

// Display list of all Offsprings.
exports.offspring_list = function(req, res, next) {

  Offspring.find()
    .populate('employee')
    .exec(function (err, list_offsprings) {
      if (err) { return next(err); }
      // Successful, so render
      res.render('offspring_list', { title: 'Offspring List', offspring_list: list_offsprings });
    });

};

// Display detail page for a specific Offspring.
exports.offspring_detail = function(req, res, next) {

    Offspring.findById(req.params.id)
    .populate('employee')
    .exec(function (err, offspring) {
      if (err) { return next(err); }
      if (offspring==null) { // No results.
          var err = new Error('Offspring not found');
          err.status = 404;
          return next(err);
        }
      // Successful, so render.
      res.render('offspring_detail', { title: 'Offspring:', offspring:  offspring});
    })

};

// Display Offspring create form on GET.
exports.offspring_create_get = function(req, res, next) {

    Employee.find({},'first_name family_name')
    .exec(function (err, employees) {
      if (err) { return next(err); }
      // Successful, so render.
      res.render('offspring_form', {title: 'Create Offspring', employee_list:employees});
    });

};

// Handle Offspring create on POST.
exports.offspring_create_post = [

    // Validate fields.
    body('employee', 'Employee must be specified').isLength({ min: 1 }).trim(),
    body('first_name', 'First name must be specified').isLength({ min: 1 }).trim(),
    body('date_of_birth', 'Invalid date').optional({ checkFalsy: true }).isISO8601(),

    // Sanitize fields.
    sanitizeBody('employee').trim().escape(),
    sanitizeBody('first_name').trim().escape(),
    sanitizeBody('date_of_birth').toDate(),

    // Process request after validation and sanitization.
    (req, res, next) => {

        // Extract the validation errors from a request.
        const errors = validationResult(req);

        // Create a Offspring object with escaped and trimmed data.
        var offspring = new Offspring(
          { employee: req.body.employee,
            first_name: req.body.first_name,
            date_of_birth: req.body.date_of_birth
           });

        if (!errors.isEmpty()) {
            // There are errors. Render form again with sanitized values and error messages.
            Employee.find({},'employee_number')
                .exec(function (err, employees) {
                    if (err) { return next(err); }
                    // Successful, so render.
                    res.render('offspring_form', { title: 'Create Offspring', employee_list : employees, selected_employee : offspring.employee._id , errors: errors.array(), offspring:offspring });
            });
            return;
        }
        else {
            // Data from form is valid.
            offspring.save(function (err) {
                if (err) { return next(err); }
                   // Successful - redirect to new record.
                   res.redirect(offspring.url);
                });
        }
    }
];

// Display Offspring delete form on GET.
exports.offspring_delete_get = function(req, res, next) {

  async.parallel({
    offspring: function(callback) {
      Offspring.findById(req.params.id).exec(callback)
    },
  }, function (err, results) {
    if (err) {return next(err); }
    if (results.offspring==null) {
      res.redirect('/catalog/offsprings');
    }
    res.render('offspring_delete', { title: 'Delete Offspring', offspring: results.offspring } );
  });

};

// Handle Offspring delete on POST.
exports.offspring_delete_post = function(req, res, next) {

  async.parallel({
    offspring: function(callback) {
      Offspring.findById(req.body.offspringid).exec(callback)
    },
  }, function (err, results) {
    if (err) { return next(err); }
    Offspring.findByIdAndRemove(req.body.offspringid, function deleteOffspring(err) {
      if (err) { return next(err); }
      res.redirect('/catalog/offsprings');
    })
  });

};

// Display Offspring update form on GET.
exports.offspring_update_get = function(req, res, next) {

    async.parallel({
      offspring: function(callback) {
        Offspring.findById(req.params.id).populate('employee').exec(callback);
      },
      employee_list: function(callback) {
        Employe.find(callback);
      },
    }, function(err, results) {
      if (err) { return next(err); }
      if (results.offspring==null) {
        var err = new Error('Offspring not found');
        err.status = 404;
        return next(err);
      }

      res.render('offspring_form', { title: 'Update Offspring', employee_list: results.employee_list, offspring: results.offspring });
    });

};

// Handle offspring update on POST.
exports.offspring_update_post = [

    // Validate fields.
    body('employee', 'Employee must not be empty.').isLength({ min: 1 }).trim(),
    body('first_name', 'First name must not be empty.').isLength({ min: 1 }).trim(),

    // Sanitize fields.
    sanitizeBody('employee').trim().escape(),
    sanitizeBody('first_name').trim().escape(),

    // Process request after validation and sanitization.
    (req, res, next) => {

        // Extract the validation errors from a request.
        const errors = validationResult(req);

        // Create a Employee object with escaped/trimmed data and old id.
        var offspring = new Offspring(
          { employee: req.body.employee,
            first_name: req.body.first_name,
            date_of_birth: req.body.date_of_birth,
            _id:req.params.id //This is required, or a new ID will be assigned!
           });

        if (!errors.isEmpty()) {
            // There are errors. Render form again with sanitized values/error messages.

            // Get all authors and genres for form.
            async.parallel({
                employee_list: function(callback) {
                    Employee.find(callback);
                },
            }, function(err, results) {
                if (err) { return next(err); }

                res.render('offspring_form', { title: 'Update Offspring', employee_list:results.employee_list, offspring: offspring });
            });
            return;
        }
        else {
            // Data from form is valid. Update the record.
            Offspring.findByIdAndUpdate(req.params.id, offspring, {}, function (err,theoffspring) {
                if (err) { return next(err); }
                   // Successful - redirect to offspring detail page.
                   res.redirect(theoffspring.url);
                });
        }
    }
];
