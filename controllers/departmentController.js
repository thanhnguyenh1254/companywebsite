const { body,validationResult } = require('express-validator/check');
const { sanitizeBody } = require('express-validator/filter');

var Employee = require('../models/employee');
var async = require('async');

var Department = require('../models/department');

// Display list of all Genre.
exports.department_list = function(req, res, next) {

  Department.find()
    .sort([['name', 'ascending']])
    .exec(function (err, list_department) {
      if (err) { return next(err); }
      res.render('department_list', { title: 'Department List', department_list: list_department });
    });

};

// Display detail page for a specific Genre.
exports.department_detail = function(req, res, next) {

    async.parallel({
        department: function(callback) {
            Department.findById(req.params.id)
              .exec(callback);
        },

        department_employees: function(callback) {
          Employee.find({ 'department': req.params.id })
          .exec(callback);
        },

    }, function(err, results) {
        if (err) { return next(err); }
        if (results.department==null) { // No results.
            var err = new Error('Department not found');
            err.status = 404;
            return next(err);
        }
        // Successful, so render
        res.render('department_detail', { title: 'Department Detail', department: results.department, department_employees: results.department_employees } );
    });

};

// Display Genre create form on GET.
exports.department_create_get = function(req, res, next) {
    res.render('department_form', { title: 'Create Department' });
};

// Handle Genre create on POST.
exports.department_create_post =  [

    // Validate that the name field is not empty.
    body('name', 'Department name required').isLength({ min: 1 }).trim(),

    // Sanitize (trim and escape) the name field.
    sanitizeBody('name').trim().escape(),

    // Process request after validation and sanitization.
    (req, res, next) => {

        // Extract the validation errors from a request.
        const errors = validationResult(req);

        // Create a department object with escaped and trimmed data.
        var department = new Department(
          { name: req.body.name }
        );


        if (!errors.isEmpty()) {
            // There are errors. Render the form again with sanitized values/error messages.
            res.render('department_form', { title: 'Create Department', department: department, errors: errors.array()});
        return;
        }
        else {
            // Data from form is valid.
            // Check if Genre with same name already exists.
            Department.findOne({ 'name': req.body.name })
                .exec( function(err, found_department) {
                     if (err) { return next(err); }

                     if (found_department) {
                         // Department exists, redirect to its detail page.
                         res.redirect(found_department.url);
                     }
                     else {

                         department.save(function (err) {
                           if (err) { return next(err); }
                           // Genre saved. Redirect to department detail page.
                           res.redirect(department.url);
                         });

                     }

                 });
        }
    }
];

// Display Genre delete form on GET.
exports.department_delete_get = function(req, res, next) {

  async.parallel({
    department: function(callback) {
      Department.findById(req.params.id).exec(callback)
    },
    department_employees: function(callback) {
      Employee.find({ 'department': req.params.id }).exec(callback)
    },
  }, function(err, results) {
    if (err) { return next(err); }
    if (results.department == null) {
      res.redirect('/catalog/departments');
    }

    res.render('department_delete', {title: 'Delete Department', department: results.department, department_employees: results.department_employees} );
  });

};

// Handle Department delete on POST.
exports.department_delete_post = function(req, res, next) {
    async.parallel({
      department: function(callback) {
        Department.findById(req.body.departmentid).exec(callback)
      },
      department_employees: function(callback) {
        Employee.find({ 'department': req.params.id }).exec(callback)
      },
    }, function(err, results) {
      if (err) { return next(err); }

      if (results.department_employees.length > 0) {
        res.render('department_delete', {title: 'Delete Department', department: results.department, department_employees: results.department_employees } );
        return;
      }
      else {
        Department.findByIdAndRemove(req.body.departmentid, function deleteDepartment(err) {
          if (err) {return next(err); }
          res.redirect('/catalog/departments')
        })
      }
    });
};

// Display Department update form on GET.
exports.department_update_get = function(req, res, next) {

  async.parallel({
    department: function(callback) {
      Department.findById(req.params.id).exec(callback);
    },
  }, function (err, results) {
    if (err) { return next(err); }
    if (results.department==null) {
      var err = new Error('Department not found');
      err.status = 404;
      return next(err);
    }

    res.render('department_form', { title: 'Update Department', department: results.department});
  });

};

// Handle Department update on POST.
exports.department_update_post = [

  body('name', 'Department name must not be empty').isLength({ min: 1}).trim(),

  sanitizeBody('name').trim().escape(),

  (req, res, next) => {

    const errors = validationResult(req);

    var department = new Department(
      { name: req.body.name,
        _id: req.params.id
      });

      if (!errors.isEmpty()) {
        res.render('department_form', {title: 'Update Department', department: department, errors: errors.array() });
        return;
      }
      else {
        Department.findByIdAndUpdate(req.params.id, department, {}, function (err,thedepartment) {
          if (err) { return next(err); }
          res.redirect(thedepartment.url);
        });
      }
  }

];
