#! /usr/bin/env node

console.log('This script populates some test employees, managers, departments and offsprings to your database. Specified database as argument - e.g.: populatedb mongodb://your_username:your_password@your_dabase_url');

// Get arguments passed on command line
var userArgs = process.argv.slice(2);
if (!userArgs[0].startsWith('mongodb://')) {
    console.log('ERROR: You need to specify a valid mongodb URL as the first argument');
    return
}

var async = require('async')
var Employee = require('./models/employee')
var Manager = require('./models/manager')
var Department = require('./models/department')
var Offspring = require('./models/offspring')


var mongoose = require('mongoose');
var mongoDB = userArgs[0];
mongoose.connect(mongoDB);
mongoose.Promise = global.Promise;
var db = mongoose.connection;
mongoose.connection.on('error', console.error.bind(console, 'MongoDB connection error:'));

var managers = []
var departments = []
var employees = []
var offsprings = []

function managerCreate(first_name, family_name, d_birth, department, cb) {
  managerdetail = {first_name:first_name , family_name: family_name }
  if (d_birth != false) managerdetail.date_of_birth = d_birth
  if (department != false) managerdetail.department = department

  var manager = new Manager(managerdetail);

  manager.save(function (err) {
    if (err) {
      cb(err, null)
      return
    }
    console.log('New Manager: ' + manager);
    managers.push(manager)
    cb(null, manager)
  }  );
}

function departmentCreate(name, cb) {
  departmentdetail = { name:name }

  var department = new Department(departmentdetail);

  department.save(function (err) {
    if (err) {
      cb(err, null);
      return;
    }
    console.log('New Department: ' + department);
    departments.push(department)
    cb(null, department);
  }   );
}

function employeeCreate(first_name, family_name, manager, date_of_birth, employee_number, department, cb) {
  employeedetail = {
    first_name: first_name,
    family_name: family_name,
    manager: manager,
    date_of_birth: date_of_birth,
    employee_number: employee_number
  }
  if (department != false) employeedetail.department = department

  var employee = new Employee(employeedetail);
  employee.save(function (err) {
    if (err) {
      cb(err, null)
      return
    }
    console.log('New Employee: ' + employee);
    employees.push(employee)
    cb(null, employee)
  }  );
}


function offSpringCreate(employee, first_name, date_of_birth, cb) {
  offspringdetail = {
    employee: employee,
    first_name: first_name,
    date_of_birth: date_of_birth
  }

  var offspring = new Offspring(offspringdetail);
  offspring.save(function (err) {
    if (err) {
      console.log('ERROR CREATING Offspring: ' + offspring);
      cb(err, null)
      return
    }
    console.log('New Offspring: ' + offspring);
    offsprings.push(offspring)
    cb(null, employee)
  }  );
}

function createDepartments(cb) {
    async.parallel([
      function(callback) {
        departmentCreate('IT', callback);
      },
      function(callback) {
        departmentCreate('Marketing', callback);
      },
      function(callback) {
        departmentCreate('Human Resources', callback);
      },
      ],
      // optional callback
      cb);
}

function createDepartmentManagers(cb) {
    async.parallel([
        function(callback) {
          managerCreate('Patrick', 'Rothfuss', '1980-06-06', [departments[0],], callback);
        },
        function(callback) {
          managerCreate('Ben', 'Bova', '1985-11-8', [departments[1],], callback);
        },
        function(callback) {
          managerCreate('Isaac', 'Asimov', '1987-01-02', [departments[2],], callback);
        },
        function(callback) {
          managerCreate('Nick', 'Ng', '1996-01-01', [departments[2],], callback);
        },
        ],
        // optional callback
        cb);
}


function createEmployees(cb) {
    async.parallel([
        function(callback) {
          employeeCreate('Adam', 'Levine', managers[0], '1990-02-03', '123456789',[departments[0],], callback);
        },
        function(callback) {
          employeeCreate('Nickel', 'Back', managers[0], '1991-04-05', '124567321', [departments[0],], callback);
        },
        function(callback) {
          employeeCreate('George', 'Barack', managers[0], '1992-09-08', '171717171', [departments[0],], callback);
        },
        function(callback) {
          employeeCreate('Chris', 'Doe', managers[1], '1990-03-06', '161616168', [departments[1],], callback);
        },
        function(callback) {
          employeeCreate('Simon', 'Hex', managers[1], '1988-08-08', '898989890', [departments[1],], callback);
        },
        function(callback) {
          employeeCreate('Danny', 'Cool', managers[3], '1996-09-09', '777777777', [departments[2],], callback);
        }
        ],
        // optional callback
        cb);
}


function createOffsprings(cb) {
    async.parallel([
        function(callback) {
          offSpringCreate(employees[0], 'London', '2001-05-06', callback)
        },
        function(callback) {
          offSpringCreate(employees[1], 'Gollancz', '1999-05-05', callback)
        },
        function(callback) {
          offSpringCreate(employees[1], 'Ken', '2005-09-09', callback)
        },
        function(callback) {
          offSpringCreate(employees[2], 'Tom', '2010-08-07', callback)
        },
        function(callback) {
          offSpringCreate(employees[3], 'Lisa', '2011-04-03', callback)
        },
        function(callback) {
          offSpringCreate(employees[3], 'Susan', '1998-06-06', callback)
        }
        ],
        // Optional callback
        cb);
}



async.series([
    createDepartments,
    createDepartmentManagers,
    createEmployees,
    createOffsprings
],
// Optional callback
function(err, results) {
    if (err) {
        console.log('FINAL ERR: '+err);
    }
    else {
        console.log('OFFSprings: '+offsprings);

    }
    // All done, disconnect from database
    mongoose.connection.close();
});
