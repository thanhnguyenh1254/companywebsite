var express = require('express');
var router = express.Router();

// Require controller modules.
var employee_controller = require('../controllers/employeeController');
var manager_controller = require('../controllers/managerController');
var department_controller = require('../controllers/departmentController');
var off_spring_controller = require('../controllers/offspringController');

/// EMPLOYEE ROUTES ///

// GET catalog home page.
router.get('/', employee_controller.index);

// GET request for creating a Employee. NOTE This must come before routes that display Employee (uses id).
router.get('/employee/create', employee_controller.employee_create_get);

// POST request for creating Employee.
router.post('/employee/create', employee_controller.employee_create_post);

// GET request to delete Employee.
router.get('/employee/:id/delete', employee_controller.employee_delete_get);

// POST request to delete Employee.
router.post('/employee/:id/delete', employee_controller.employee_delete_post);

// GET request to update Employee.
router.get('/employee/:id/update', employee_controller.employee_update_get);

// POST request to update Employee.
router.post('/employee/:id/update', employee_controller.employee_update_post);

// GET request for one Employee.
router.get('/employee/:id', employee_controller.employee_detail);

// GET request for list of all Employee.
router.get('/employees', employee_controller.employee_list);

/// MANAGER ROUTES ///

// GET request for creating Manager. NOTE This must come before route for id (i.e. display manager).
router.get('/manager/create', manager_controller.manager_create_get);

// POST request for creating Manager.
router.post('/manager/create', manager_controller.manager_create_post);

// GET request to delete Manager.
router.get('/manager/:id/delete', manager_controller.manager_delete_get);

// POST request to delete Manager.
router.post('/manager/:id/delete', manager_controller.manager_delete_post);

// GET request to update Manager.
router.get('/manager/:id/update', manager_controller.manager_update_get);

// POST request to update Manager.
router.post('/manager/:id/update', manager_controller.manager_update_post);

// GET request for one Manager.
router.get('/manager/:id', manager_controller.manager_detail);

// GET request for list of all Managers.
router.get('/managers', manager_controller.manager_list);

/// DEPARTMENT ROUTES ///

// GET request for creating a Department. NOTE This must come before route that displays Department (uses id).
router.get('/department/create', department_controller.department_create_get);

//POST request for creating Department.
router.post('/department/create', department_controller.department_create_post);

// GET request to delete Department.
router.get('/department/:id/delete', department_controller.department_delete_get);

// POST request to delete Department.
router.post('/department/:id/delete', department_controller.department_delete_post);

// GET request to update Department.
router.get('/department/:id/update', department_controller.department_update_get);

// POST request to update Department.
router.post('/department/:id/update', department_controller.department_update_post);

// GET request for one Department.
router.get('/department/:id', department_controller.department_detail);

// GET request for list of all Departments.
router.get('/departments', department_controller.department_list);

/// OFFSPRING ROUTES ///

// GET request for creating a Offspring. NOTE This must come before route that displays Offspring (uses id).
router.get('/offspring/create', off_spring_controller.offspring_create_get);

// POST request for creating Offspring.
router.post('/offspring/create', off_spring_controller.offspring_create_post);

// GET request to delete Offspring.
router.get('/offspring/:id/delete', off_spring_controller.offspring_delete_get);

// POST request to delete Offspring.
router.post('/offspring/:id/delete', off_spring_controller.offspring_delete_post);

// GET request to update Offspring.
router.get('/offspring/:id/update', off_spring_controller.offspring_update_get);

// POST request to update Offspring.
router.post('/offspring/:id/update', off_spring_controller.offspring_update_post);

// GET request for one Offspring.
router.get('/offspring/:id', off_spring_controller.offspring_detail);

// GET request for list of all Offsprings.
router.get('/offsprings', off_spring_controller.offspring_list);

module.exports = router;
